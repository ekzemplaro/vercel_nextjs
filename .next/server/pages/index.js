"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/index";
exports.ids = ["pages/index"];
exports.modules = {

/***/ "./locales/en.js":
/*!***********************!*\
  !*** ./locales/en.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n// en.js\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({\n    SUBTITLE: \"Visualize your customer happiness.\",\n    TO_DASHBOARD: \"Go to dashboard\",\n    DATE: \"May/25/2022 AM 10:30\",\n    GREETING: \"For 400 years since the Azuchi-Momoyama Period, when our predecessor, Zenkichi Hinaya Kuwabara Shoten, was founded, we have been operating with a high level of trust from our customers in the fields of construction, commerce, industry, and economy in Gifu Prefecture. We believe it is our mission to live up to this long-standing trust and high expectation of quality.\"\n});\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9sb2NhbGVzL2VuLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7QUFDQSxRQUFRO0FBQ1IsaUVBQWU7SUFDYkEsUUFBUSxFQUFFLG9DQUFvQztJQUM5Q0MsWUFBWSxFQUFFLGlCQUFpQjtJQUMvQkMsSUFBSSxFQUFFLHNCQUFzQjtJQUM1QkMsUUFBUSxFQUFFLGtYQUFrWDtDQUU3WCIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL2xvY2FsZXMvZW4uanM/MjlhNCJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8vIGVuLmpzXG5leHBvcnQgZGVmYXVsdCB7XG4gIFNVQlRJVExFOiBcIlZpc3VhbGl6ZSB5b3VyIGN1c3RvbWVyIGhhcHBpbmVzcy5cIixcbiAgVE9fREFTSEJPQVJEOiBcIkdvIHRvIGRhc2hib2FyZFwiLFxuICBEQVRFOiBcIk1heS8yNS8yMDIyIEFNIDEwOjMwXCIsXG4gIEdSRUVUSU5HOiBcIkZvciA0MDAgeWVhcnMgc2luY2UgdGhlIEF6dWNoaS1Nb21veWFtYSBQZXJpb2QsIHdoZW4gb3VyIHByZWRlY2Vzc29yLCBaZW5raWNoaSBIaW5heWEgS3V3YWJhcmEgU2hvdGVuLCB3YXMgZm91bmRlZCwgd2UgaGF2ZSBiZWVuIG9wZXJhdGluZyB3aXRoIGEgaGlnaCBsZXZlbCBvZiB0cnVzdCBmcm9tIG91ciBjdXN0b21lcnMgaW4gdGhlIGZpZWxkcyBvZiBjb25zdHJ1Y3Rpb24sIGNvbW1lcmNlLCBpbmR1c3RyeSwgYW5kIGVjb25vbXkgaW4gR2lmdSBQcmVmZWN0dXJlLiBXZSBiZWxpZXZlIGl0IGlzIG91ciBtaXNzaW9uIHRvIGxpdmUgdXAgdG8gdGhpcyBsb25nLXN0YW5kaW5nIHRydXN0IGFuZCBoaWdoIGV4cGVjdGF0aW9uIG9mIHF1YWxpdHkuXCIsXG4gIC8vIC4uLlxufVxuIl0sIm5hbWVzIjpbIlNVQlRJVExFIiwiVE9fREFTSEJPQVJEIiwiREFURSIsIkdSRUVUSU5HIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./locales/en.js\n");

/***/ }),

/***/ "./locales/ja.js":
/*!***********************!*\
  !*** ./locales/ja.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({\n    SUBTITLE: \"\\u300C\\u6E80\\u8DB3\\u300D\\u3092\\u53EF\\u8996\\u5316\\u3057\\u3088\\u3046\",\n    TO_DASHBOARD: \"\\u30C0\\u30C3\\u30B7\\u30E5\\u30DC\\u30FC\\u30C9\\u3078\",\n    DATE: \"2022\\u5E745\\u670825\\u65E5 \\u5348\\u524D10\\u664230\\u5206\",\n    GREETING: \"\\u5F53\\u793E\\u306F\\u3001\\u524D\\u8EAB\\u3067\\u3042\\u308B\\u96DB\\u5C4B\\u6851\\u539F\\u5584\\u5409\\u5546\\u5E97\\u304C\\u5275\\u696D\\u3057\\u305F \\u5B89\\u571F\\u6843\\u5C71\\u6642\\u4EE3\\u3088\\u308A\\uFF14\\uFF10\\uFF10\\u5E74\\u306B\\u308F\\u305F\\u308A\\u3001 \\u5EFA\\u7BC9\\u3092\\u306F\\u3058\\u3081\\u5C90\\u961C\\u306E\\u5546\\u5DE5\\u696D\\u3001\\u7D4C\\u6E08\\u306B\\u304A\\u3044\\u3066\\u3001 \\u7686\\u69D8\\u306B\\u9AD8\\u3044\\u4FE1\\u983C\\u3092\\u9802\\u304D\\u3001\\u55B6\\u696D\\u3057\\u3066\\u307E\\u3044\\u308A\\u307E\\u3057\\u305F\\u3002 \\u3053\\u306E\\u6C38\\u5E74\\u306B\\u308F\\u305F\\u308B\\u4FE1\\u983C\\u3068\\u54C1\\u8CEA\\u306B\\u5BFE\\u3059\\u308B\\u9AD8\\u3044\\u671F\\u5F85\\u306B\\u304A\\u5FDC\\u3048\\u3059\\u308B\\u3053\\u3068\\u304C\\u3001 \\u5F53\\u793E\\u306E\\u4F7F\\u547D\\u3067\\u3042\\u308B\\u3068\\u8003\\u3048\\u3066\\u304A\\u308A\\u307E\\u3059\\u3002\"\n});\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9sb2NhbGVzL2phLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7QUFBQSxpRUFBZTtJQUNiQSxRQUFRLEVBQUUsb0VBQWE7SUFDREMsWUFBVixFQUFFLGtEQUFVO0lBQ1JDLElBQVosRUFBRSx3REFBcUI7SUFDYkMsUUFBTixFQUFFLDZ3QkFBd0k7Q0FFbkoiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9sb2NhbGVzL2phLmpzPzJhZGIiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGRlZmF1bHQge1xuICBTVUJUSVRMRTogXCLjgIzmuoDotrPjgI3jgpLlj6/oppbljJbjgZfjgojjgYZcIixcbiAgVE9fREFTSEJPQVJEOiBcIuODgOODg+OCt+ODpeODnOODvOODieOBuFwiLFxuICBEQVRFOiBcIjIwMjLlubQ15pyIMjXml6Ug5Y2I5YmNMTDmmYIzMOWIhlwiLFxuICBHUkVFVElORzogXCLlvZPnpL7jga/jgIHliY3ouqvjgafjgYLjgovpm5vlsYvmoZHljp/lloTlkInllYblupfjgYzlibXmpa3jgZfjgZ8g5a6J5Zyf5qGD5bGx5pmC5Luj44KI44KK77yU77yQ77yQ5bm044Gr44KP44Gf44KK44CBIOW7uuevieOCkuOBr+OBmOOCgeWykOmYnOOBruWVhuW3pealreOAgee1jOa4iOOBq+OBiuOBhOOBpuOAgSDnmobmp5jjgavpq5jjgYTkv6HpoLzjgpLpoILjgY3jgIHllrbmpa3jgZfjgabjgb7jgYTjgorjgb7jgZfjgZ/jgIIg44GT44Gu5rC45bm044Gr44KP44Gf44KL5L+h6aC844Go5ZOB6LOq44Gr5a++44GZ44KL6auY44GE5pyf5b6F44Gr44GK5b+c44GI44GZ44KL44GT44Go44GM44CBIOW9k+ekvuOBruS9v+WRveOBp+OBguOCi+OBqOiAg+OBiOOBpuOBiuOCiuOBvuOBmeOAglwiLFxuICAvLyAuLi5cbn1cbiJdLCJuYW1lcyI6WyJTVUJUSVRMRSIsIlRPX0RBU0hCT0FSRCIsIkRBVEUiLCJHUkVFVElORyJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./locales/ja.js\n");

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Home)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/router */ \"next/router\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _locales_en__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../locales/en */ \"./locales/en.js\");\n/* harmony import */ var _locales_ja__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../locales/ja */ \"./locales/ja.js\");\n\n\n\n\nfunction Home() {\n    const { locale  } = (0,next_router__WEBPACK_IMPORTED_MODULE_1__.useRouter)();\n    const t = locale === \"en\" ? _locales_en__WEBPACK_IMPORTED_MODULE_2__[\"default\"] : _locales_ja__WEBPACK_IMPORTED_MODULE_3__[\"default\"];\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        children: [\n            \"...\",\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"p\", {\n                children: t.GREETING\n            }, void 0, false, {\n                fileName: \"/home/uchida/html/gitlab/vercel_nextjs/pages/index.js\",\n                lineNumber: 13,\n                columnNumber: 3\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h2\", {\n                children: t.SUBTITLE\n            }, void 0, false, {\n                fileName: \"/home/uchida/html/gitlab/vercel_nextjs/pages/index.js\",\n                lineNumber: 14,\n                columnNumber: 3\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"button\", {\n                children: t.TO_DASHBOARD\n            }, void 0, false, {\n                fileName: \"/home/uchida/html/gitlab/vercel_nextjs/pages/index.js\",\n                lineNumber: 15,\n                columnNumber: 3\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"p\", {\n                children: t.DATE\n            }, void 0, false, {\n                fileName: \"/home/uchida/html/gitlab/vercel_nextjs/pages/index.js\",\n                lineNumber: 16,\n                columnNumber: 3\n            }, this)\n        ]\n    }, void 0, true, {\n        fileName: \"/home/uchida/html/gitlab/vercel_nextjs/pages/index.js\",\n        lineNumber: 11,\n        columnNumber: 7\n    }, this);\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9pbmRleC5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7QUFBd0M7QUFDVDtBQUNBO0FBRWhCLFNBQVNHLElBQUksR0FBRztJQUM1QixNQUFNLEVBQUVDLE1BQU0sR0FBRSxHQUFHSixzREFBUyxFQUFFO0lBRTlCLE1BQU1LLENBQUMsR0FBR0QsTUFBTSxLQUFLLElBQUksR0FBR0gsbURBQUUsR0FBR0MsbURBQUU7SUFFbkMscUJBQ0csOERBQUNJLEtBQUc7O1lBQUMsS0FFVDswQkFBQSw4REFBQ0MsR0FBQzswQkFBRUYsQ0FBQyxDQUFDRyxRQUFROzs7OztvQkFBSzswQkFDbkIsOERBQUNDLElBQUU7MEJBQUVKLENBQUMsQ0FBQ0ssUUFBUTs7Ozs7b0JBQU07MEJBQ3JCLDhEQUFDQyxRQUFNOzBCQUFFTixDQUFDLENBQUNPLFlBQVk7Ozs7O29CQUFVOzBCQUNqQyw4REFBQ0wsR0FBQzswQkFBRUYsQ0FBQyxDQUFDUSxJQUFJOzs7OztvQkFBSzs7Ozs7O1lBQ0wsQ0FDUjtDQUNIIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vcGFnZXMvaW5kZXguanM/YmVlNyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tIFwibmV4dC9yb3V0ZXJcIjtcbmltcG9ydCBlbiBmcm9tIFwiLi4vbG9jYWxlcy9lblwiO1xuaW1wb3J0IGphIGZyb20gXCIuLi9sb2NhbGVzL2phXCI7XG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEhvbWUoKSB7XG4gICBjb25zdCB7IGxvY2FsZSB9ID0gdXNlUm91dGVyKClcbiAgIFxuICAgY29uc3QgdCA9IGxvY2FsZSA9PT0gXCJlblwiID8gZW4gOiBqYTtcbiAgIFxuICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgICAuLi5cblx0IDxwPnt0LkdSRUVUSU5HfTwvcD5cblx0IDxoMj57dC5TVUJUSVRMRX08L2gyPlxuXHQgPGJ1dHRvbj57dC5UT19EQVNIQk9BUkR9PC9idXR0b24+XG5cdCA8cD57dC5EQVRFfTwvcD5cbiAgICAgIDwvZGl2PlxuICAgKVxufVxuIl0sIm5hbWVzIjpbInVzZVJvdXRlciIsImVuIiwiamEiLCJIb21lIiwibG9jYWxlIiwidCIsImRpdiIsInAiLCJHUkVFVElORyIsImgyIiwiU1VCVElUTEUiLCJidXR0b24iLCJUT19EQVNIQk9BUkQiLCJEQVRFIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/index.js\n");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

module.exports = require("react/jsx-dev-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/index.js"));
module.exports = __webpack_exports__;

})();