
// en.js
export default {
  SUBTITLE: "Visualize your customer happiness.",
  TO_DASHBOARD: "Go to dashboard",
  DATE: "May/25/2022 AM 10:50",
  GREETING: "For 400 years since the Azuchi-Momoyama Period, when our predecessor, Zenkichi Hinaya Kuwabara Shoten, was founded, we have been operating with a high level of trust from our customers in the fields of construction, commerce, industry, and economy in Gifu Prefecture. We believe it is our mission to live up to this long-standing trust and high expectation of quality.",
  // ...
}
