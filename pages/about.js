import { useRouter } from "next/router";
import en from "../locales/en";
import ja from "../locales/ja";

export default function Home() {
   const { locale } = useRouter()
   
   const t = locale === "en" ? en : ja;
   
   return (
      <div>
         ...
	 <p>{t.GREETING}</p>
	<p></p>
	 <p>{t.DATE}</p>
      </div>
   )
}
